from quart import Quart, jsonify, render_template
from random import randint

app = Quart(__name__)

@app.route('/')
async def index():
  return await render_template('index.html')
  
@app.route('/input', methods=['GET', 'POST'])
def get_number():
  return jsonify(f"{randint(1,100)}")


if __name__ == "__main__":
  app.run(host='0.0.0.0',port=8000)

